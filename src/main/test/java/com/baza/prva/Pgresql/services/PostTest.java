package com.baza.prva.Pgresql.services;

import com.baza.prva.Pgresql.model.Client;
import com.baza.prva.Pgresql.controller.WebController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static sun.plugin2.util.PojoUtil.toJson;

@RunWith(SpringRunner.class)
@WebMvcTest(WebController.class)
public class PostTest {


    @Autowired
    MockMvc mvc;
    @MockBean
    ClientServices employeeService;

    @Test
    public void addEmployeeTest() throws Exception {

        Optional<Client> client = employeeService.create(new Client(5, "mile1", "mileta dragica", "21133"));

        mvc.perform(post("/createClient")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(client)))
                .andExpect(status().isOk());
    }
}

