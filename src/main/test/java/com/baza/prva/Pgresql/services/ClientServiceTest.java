package com.baza.prva.Pgresql.services;

import com.baza.prva.Pgresql.model.Client;
import com.baza.prva.Pgresql.repo.SpringRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceTest {


    @Mock
    private SpringRepository clientRepository;
    @InjectMocks
    private ClientServices underTest;

    @Test
    public void testGetOne() {
        Long id = 1l;
        Client expected = new Client();

        when(clientRepository.findById(1L)).thenReturn(Optional.of(expected));

        Optional<Client> actual = underTest.getOne(id);

        assertTrue(actual.isPresent());
        assertEquals(actual.get(), expected);
    }

    @Test
    public void testGetAll() {
        List<Client> clientList = new ArrayList<>();
        clientList.add(new Client(1, "mile1", "mileta dragica", "21133"));
        clientList.add(new Client(2, "mile2", "mileta dragica", "21133"));
        clientList.add(new Client(3, "mile3", "mileta dragica", "21133"));
        clientList.add(new Client(4, "mile4", "mileta dragica", "21133"));

        when(clientRepository.findAll()).thenReturn(clientList);

        List<Client> actual = underTest.getAll();

        assertTrue(!actual.isEmpty());
        assertEquals(actual.getClass(), clientList.getClass());
        assertEquals(actual.get(3), clientList.get(3));

    }

    @Test
    public void testSave() {
        Client expected = new Client(1, "mile1", "mileta dragica", "21133");

        when(clientRepository.save(expected)).thenReturn(expected);

        Optional<Client> actual = underTest.create(expected);
        assertTrue(actual.isPresent());
        assertEquals(actual.get().getClass(), expected.getClass());
        assertEquals(actual.get(), expected);

    }

    @Test
    public void testSaveWithVerify() {
//prepare
        Long personId = 1l;
        Client person = new Client(personId, "mile1", "mileta dragica", "21133");
        when(clientRepository.save(person)).thenReturn(person);
//excersize
        underTest.create(person);
//verify
        verify(clientRepository, times(1)).save(person);
    }

    @Test
    public void deleteTest() {

        Long personId = 1l;
        Client person = new Client(personId, "mile1", "mileta dragica", "21133");

        Mockito.when(clientRepository.findById(personId)).thenReturn(Optional.of(person)); //expect a fetch, return a "fetched" person;

        Optional<Client> actual = underTest.getOne(personId);

        assertTrue(actual.isPresent());

        underTest.delete(personId);

        verify(clientRepository, times(1)).deleteById(person.getId()); //pretty sure it is verify after call
    }
}