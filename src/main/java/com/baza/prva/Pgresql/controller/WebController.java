package com.baza.prva.Pgresql.controller;

import com.baza.prva.Pgresql.model.Client;
import com.baza.prva.Pgresql.repo.SpringRepository;
import com.baza.prva.Pgresql.services.ClientServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@Transactional
@EnableJpaRepositories(basePackageClasses = SpringRepository.class)
public class WebController {

    @Autowired
    private ClientServices restServices;

    @GetMapping("/clients")
    public ResponseEntity getClientsFromBase() {
        return ResponseEntity.ok(restServices.getAll());
    }

    @GetMapping("/client/{id}")
    public ResponseEntity getClientFromBase(@PathVariable long id) {
        return ResponseEntity.ok(restServices.getOne(id));
    }

    @PostMapping("/saveClient")
    public ResponseEntity postIntoBase(@RequestBody @Valid Client client) {
        return ResponseEntity.ok(restServices.create(client));
    }

    @PutMapping("/updateClient/{id}")
    public ResponseEntity updateBase(@PathVariable long id, @RequestBody @Valid Client client) {
        return ResponseEntity.ok(restServices.update(client, id));
    }

    @DeleteMapping("/removeClient/{id}")
    public void removeFromBase(@PathVariable long id) {
        restServices.delete(id);
    }


}
