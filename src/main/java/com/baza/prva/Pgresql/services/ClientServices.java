package com.baza.prva.Pgresql.services;

import com.baza.prva.Pgresql.model.Client;
import com.baza.prva.Pgresql.exception.MyResourceNotFoundException;
import com.baza.prva.Pgresql.repo.SpringRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class ClientServices implements ClientServiceInterface {

    @Autowired
    private SpringRepository springRepository;


    @Override
    public Optional<Client> getOne(Long id){
        Optional <Client> klija1= springRepository.findById(id);
        if (!klija1.isPresent()) {
            throw new MyResourceNotFoundException("Client non existent!");
        }
        return klija1;
    }

    @Override
    public List<Client> getAll() {
        List<Client> clients = (List<Client>) springRepository.findAll();
//        clients.clear();
        Collections.reverse(clients);
        return clients;
    }

    @Override
    public Optional <Client> create(Client client) {
       Client client33 = springRepository.save(client);

        return Optional.of(client33);
    }

    @Override
    public Optional <Client> update(Client client, long id) {
        Optional<Client> current = springRepository.findById(id);
        if (!current.isPresent()) {
            throw new MyResourceNotFoundException("Client non existent!");
        }
        current.get().setIme(client.getIme());
        current.get().setAdresa(client.getAdresa());
        current.get().setBrtel(client.getBrtel());

        return current;
    }

    @Override
    public void delete(Long id){
        Optional<Client> current = springRepository.findById(id);
        if (!current.isPresent()) {
            throw new MyResourceNotFoundException("Client non existent!");
        }
        springRepository.deleteById(id);
    }
}

