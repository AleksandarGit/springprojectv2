package com.baza.prva.Pgresql.services;

import com.baza.prva.Pgresql.model.Client;

import java.util.List;
import java.util.Optional;

public interface ClientServiceInterface {
    Optional<Client> getOne(Long id);
    List<Client> getAll();
    Optional <Client> create(Client client);
    Optional <Client> update(Client client, long id);
    void delete(Long id);
}
