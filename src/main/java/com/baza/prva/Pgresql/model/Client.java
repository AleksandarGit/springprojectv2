package com.baza.prva.Pgresql.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
public class Client implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    private String ime;
    @NotNull
    private String adresa;

    private String brtel;

    public Client() {
    }

    public Client(String ime, String adresa, String brtel) {
        this.ime = ime;
        this.adresa = adresa;
        this.brtel = brtel;
    }
    public Client(long id, String ime, String adresa, String brtel) {
        this.id = id;
        this.ime = ime;
        this.adresa = adresa;
        this.brtel = brtel;
    }
    public long getId() {
        return id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getBrtel() {
        return brtel;
    }

    public void setBrtel(String brtel) {
        this.brtel = brtel;
    }

    @Override
    public String toString() {
        return "Client{" +
                "ime='" + ime + '\'' +
                ", adresa='" + adresa + '\'' +
                ", brtel='" + brtel + '\'' +
                '}';
    }
}
