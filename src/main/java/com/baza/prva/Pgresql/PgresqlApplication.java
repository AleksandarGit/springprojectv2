package com.baza.prva.Pgresql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PgresqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(PgresqlApplication.class, args);
	}
}
