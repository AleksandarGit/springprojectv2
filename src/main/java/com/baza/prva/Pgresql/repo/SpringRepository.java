package com.baza.prva.Pgresql.repo;

import com.baza.prva.Pgresql.model.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpringRepository extends CrudRepository<Client, Long> {

}